from time import gmtime, strftime

from dea.bulk import CdoImportDefinition
from dea.bulk.api.cdo import BulkCdoClient
from dea.bulk.definitions import MapDataCardsConfig
from dea.bulk.eml import eml
from dea.rest.api.cdo import RestCdoClient
from requests.auth import HTTPBasicAuth

from logging_config import setup_logging
from time import sleep
import collections

logger = setup_logging(__name__)


class ElqClient:
    def __init__(self, elq_user, elq_password, elq_base_url):
        self.bulk_client = BulkCdoClient(auth=HTTPBasicAuth(username=elq_user, password=elq_password),
                                         base_url=elq_base_url)
        self.rest_client = RestCdoClient(auth=HTTPBasicAuth(username=elq_user, password=elq_password),
                                         base_url=elq_base_url)

    def find_elq_cdo(self, cdo_name):

        response = self.rest_client.get(
            self.rest_client.rest_url_for("/assets/customObjects?search=name={}".format(cdo_name)))
        if len(response["elements"]) == 1:
            for element in response["elements"]:
                logger.debug("Found CDO with ID {}".format(element["id"]))
                return element["id"]
        else:
            return False

    def create_cdo(self, cdo_name, vdl_fields):
        fields = []
        for vdl_field in vdl_fields:
            for field_name in vdl_field:
                field = {
                    "name": field_name,
                    "defaultValue": "",
                    "displayType": "text",
                    "dataType": vdl_field[field_name]
                }

                fields.append(field)
        payload = {
            "name": cdo_name,
            "description": "Visma data lake cdo for {}".format(cdo_name),
            "fields": fields
        }
        response = self.rest_client.post(self.rest_client.rest_url_for("/assets/customObject"), json=payload)
        return response["id"]

    def update_cdo(self, cdo_id, cdo_name, fields):
        response = self.rest_client.get(self.rest_client.rest_url_for("/assets/customObject/{}".format(cdo_id)))
        cdo_fields = []
        vdl_fields = []
        updated_fields = []
        for f in response["fields"]:
            cdo_fields.append(f["name"])
        for vdl_field in fields:
            for field_name in vdl_field:
                vdl_fields.append(field_name)
        if collections.Counter(cdo_fields) != collections.Counter(vdl_fields):
            payload = {
                "id": cdo_id,
                "name": cdo_name,
                "description": "Visma data lake cdo for {}".format(cdo_name),
                "fields": []
            }

            self.rest_client.put(self.rest_client.rest_url_for("/assets/customObject/{}".format(cdo_id)),
                                 json=payload)
            for vdl_field in fields:
                for field_name in vdl_field:
                    field = {
                        "name": field_name,
                        "defaultValue": "",
                        "displayType": "text",
                        "dataType": vdl_field[field_name]
                    }
                    updated_fields.append(field)

            payload = {
                "id": cdo_id,
                "name": cdo_name,
                "description": "Visma data lake cdo for {}".format(cdo_name),
                "fields": updated_fields
            }
            self.rest_client.put(self.rest_client.rest_url_for("/assets/customObject/{}".format(cdo_id)),
                                 json=payload)
            logger.debug("Update complete for {}".format(cdo_name))
        else:
            logger.debug("Nothing to update for {}".format(cdo_name))

    def create_import_def(self, cdo_id, fields):
        import_def = {}
        response = self.bulk_client.get(self.bulk_client.bulk_url_for("/customObjects/{}/fields".format(cdo_id)))
        for field in fields:
            for cdo_field in response["items"]:
                if cdo_field["name"] in field:
                    import_def[cdo_field["name"]] = cdo_field["statement"]

        return import_def

    def import_data(self, data, import_def, cdo_id):
        map_data_card = MapDataCardsConfig(entity_type="Contact", entity_field=eml.Contact.Field("C_EmailAddress"),
                                           source_field="Email")
        cdo_import_def = CdoImportDefinition(name="VDL import definition", fields=import_def, id_field_name="Email",
                                             parent_id=cdo_id, trigger_sync_on_import=True,
                                             map_data_cards=map_data_card)
        with self.bulk_client.bulk_cdo.imports.create_import(import_def=cdo_import_def,
                                                             parent_id=cdo_id) as bulk_import:
            bulk_import.add_items(data)
            bulk_import.upload_and_flush_data(sync_on_upload=True)
        logger.debug("Bulk import complete")

    def flush_cdo_data(self, cdo_id):
        while True:
            response = self.rest_client.get(
                self.rest_client.rest_url_for("/data/customObject/{}/instances".format(cdo_id)))
            for cdo_record in response["elements"]:
                self.rest_client.delete(self.rest_client.rest_url_for(
                    "/data/customObject/{0}/instance/{1}".format(cdo_id, cdo_record["id"])))
            if len(response["elements"]) == 0:
                logger.debug("CDO record flushed")
                return True
            else:
                sleep(5)

    def import_to_contact_cdo(self, cdo_id, import_def, data):

        current_timestamp = strftime("%m/%d/%Y %H:%M", gmtime())
        for row in data:
            for field in list(row.keys()):
                if field not in list(import_def.keys()):
                    row.pop(field)
                row["integration_source"] = "VDL-Eloqua-integration"
                row["integration_date"] = current_timestamp
        if len(data) > 0:
            logger.debug("Importing contacts to contact information cdo")
            self.import_data(cdo_id=cdo_id, data=data, import_def=import_def)
