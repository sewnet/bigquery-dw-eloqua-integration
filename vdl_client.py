import datetime
import json
import os

from google.cloud import bigquery, logging

import eloqua_config
import settings
import vdl_config
from eloqua_client import ElqClient
from logging_config import setup_logging

logger = setup_logging(__name__)
vdl_client = bigquery.Client()
google_logging_client = logging.Client()
google_logger = google_logging_client.logger("VDL-Eloqua-integration-program-log")


def main():
    datasets = list(vdl_client.list_datasets())
    project = vdl_client.project
    if datasets:
        logger.info("Datasets in project {}:".format(project))
        for dataset in datasets:
            logger.debug("\t{}".format(dataset.dataset_id))
            google_logger.log_text("\t{}".format(dataset.dataset_id), severity="INFO")

            dataset = vdl_client.get_dataset(dataset.dataset_id)
            tables = list(vdl_client.list_tables(dataset))  # Make an API request(s).
            if tables:
                google_logger.log_text("\t Found {} tables in {} Dataset".format(len(tables), dataset.dataset_id),
                                       severity="INFO")
                for table in tables:
                    if table.table_id not in vdl_config.exclude_tables and table.table_id != vdl_config.logs_table:
                        logger.debug("\t Grabbing data for {}".format(table.table_id))
                        google_logger.log_text("\t Grabbing data for {}".format(table.table_id), severity="INFO")
                        dataset_name = "{}.{}.".format(table.project, dataset.dataset_id)
                        try:
                            table_name = "{}.{}.{}".format(table.project, dataset.dataset_id, table.table_id)
                            query = ('SELECT * FROM `{}`'.format(table_name))
                            query_job = vdl_client.query(query)
                            vdl_table = query_job.to_dataframe()
                            table_schema = vdl_client.get_table(table_name)
                            logger.info("####################################################### {}".format(table_schema.schema))
                            elq_import(data=vdl_table, table_name=table.table_id, table_schema=table_schema.schema)
                            logs(status="Success", table=table.table_id, dataset=dataset_name)
                        except Exception as E:
                            logs(status="Failed", table=table.table_id, dataset=dataset_name)
                            logger.error("\t Failed to integrate for table {} \n Reason: {}".format(table.table_id, E))
                            google_logger.log_text(
                                "\t Failed to integrate for table {} \n Reason: {}".format(table.table_id, E),
                                severity="ERROR")
                            pass

            else:
                logger.warning("\t{} does not contain any tables.".format(dataset.dataset_id))
                google_logger.log_text("\tThis dataset does not contain any tables.", severity="ERROR")
    else:
        logger.warning("\t This project does not contain any data sets")
        google_logger.log_text("\t This project does not contain any data sets", severity="ERROR")


def elq_import(data, table_name, table_schema):
    elq_client = ElqClient(elq_user=settings.ELQ_USER, elq_password=settings.ELQ_PASSWORD,
                           elq_base_url=settings.ELQ_BASE_URL)
    formatted_data = []
    for row, values in data.to_dict(orient="index").items():
        serialized = json.dumps(values, default=json_serial)
        formatted_data.append(json.loads(serialized))

    vdl_fields = []
    for field in table_schema:
        field_type = "text"
        if field.field_type == "DATE" or field.field_type == "DATETIME":
            field_type = "date"
        elif field.field_type == "NUMERIC" or field.field_type == "INTEGER":
            field_type = "numeric"
        fields = {field.name: field_type}
        vdl_fields.append(fields)
    elq_cdo = elq_client.find_elq_cdo(table_name)
    if elq_cdo:
        google_logger.log_text("\t Found existing CDO for table: {}".format(table_name), severity="INFO")
        google_logger.log_text("\t Flushing records from CDO: {}".format(table_name), severity="INFO")
        elq_client.flush_cdo_data(cdo_id=elq_cdo)
        google_logger.log_text("\t Updating fields for {} CDO".format(table_name), severity="INFO")
        elq_client.update_cdo(cdo_name=table_name, cdo_id=elq_cdo, fields=vdl_fields)
        import_def = elq_client.create_import_def(cdo_id=elq_cdo, fields=vdl_fields)
        google_logger.log_text("\t importing Data to {} CDO".format(table_name), severity="INFO")
        elq_client.import_data(data=formatted_data, cdo_id=elq_cdo, import_def=import_def)

    else:
        logger.debug("\tCreating CDO for {}.".format(table_name))
        google_logger.log_text("\tCreating CDO for {}.".format(table_name), severity="INFO")
        create_cdo = elq_client.create_cdo(cdo_name=table_name, vdl_fields=vdl_fields)
        google_logger.log_text("\t importing Data to {} CDO".format(table_name), severity="INFO")
        import_def = elq_client.create_import_def(cdo_id=create_cdo, fields=vdl_fields)
        elq_client.import_data(data=formatted_data, cdo_id=create_cdo, import_def=import_def)
    google_logger.log_text("\tImporting contacts to contact Information CDO", severity="INFO")
    elq_client.import_to_contact_cdo(cdo_id=eloqua_config.cdo_id, data=formatted_data,
                                     import_def=eloqua_config.import_def)
    return True


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime.datetime, datetime.date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


def logs(status, table, dataset):
    table_name = "{}{}".format(dataset, vdl_config.logs_table)
    current_date = datetime.datetime.now()
    rows = [
        {'Dataset_Name': dataset,
         "Table_Name": table,
         "Date": str(current_date),
         "Status": status}
    ]
    vdl_client.insert_rows_json(table=table_name, json_rows=rows)


if __name__ == '__main__':
    logger.info("Start execution")
    google_logger.log_text("\tStart execution", severity="INFO")
    main()
    logger.info("Execution Ended")
    google_logger.log_text("\tExecution Ended", severity="INFO")
