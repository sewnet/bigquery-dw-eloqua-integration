from environs import Env

env = Env()
env.read_env()

# Visma Eloqua config
ELQ_USER = env("ELQ_USER")
ELQ_PASSWORD = env("ELQ_PASSWORD")
ELQ_BASE_URL = env("ELQ_BASE_URL")